﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Console;
using System.IO;


namespace algo_3
{
    
    public partial class Form1 : Form
    {


        [DllImport("kernel32.dll", SetLastError = true)]
        //[return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool AllocConsole();

        [DllImport("kernel32.dll", SetLastError = true)]
        //[return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool FreeConsole();

        string str;

        static void Swap(ref int e1, ref int e2)
        {
            var temp = e1;
            e1 = e2;
            e2 = temp;
        }

        static int[] BubbleSort(int[] array)
        {
            var len = array.Length;
            for (var i = 1; i < len; i++)
            {
                for (var j = 0; j < len - i; j++)
                {
                    if (array[j] < array[j + 1])
                    {
                        Swap(ref array[j], ref array[j + 1]);
                    }
                }
            }

            return array;
        }


        double f2 = 0;
        int f1 = 1;
        double b1 = 0;
        public Form1()
        {
            
            
            InitializeComponent();
            while (true)
            {

                AllocConsole();
                Stopwatch time;
                time = Stopwatch.StartNew();
                Console.WriteLine("Числа Фибоначчи");
                Random rnd = new Random();
                int n = rnd.Next(2, 90);
                int u, u1 = 1, u2 = 0;
                

                Console.WriteLine(0);
                for (u = 1; ; u += 1)
                {
                    u = u1 + u2;
                    f1++;
                    if (u >= n)
                        break;
                    u1 = u2;
                    u2 = u;
                    Console.WriteLine(u);
                }
                time.Stop();
                f2 = time.Elapsed.TotalSeconds;
                Console.WriteLine($"Час для Фибоначчи = {time.Elapsed.TotalSeconds} s");

                Console.WriteLine(0);

                Console.WriteLine("Сортування бульбашкою");
                int [] array = new int [1024];
                int coount = 0;
                for (int i = 0; i < 1024; i++)
                {
                    array[i] = rnd.Next(-1024, 1024);
                    coount++;
                }

                Console.WriteLine($"Невідсортований масив");
                foreach (var f in array)
                {
                    Console.WriteLine(f);
                }
                time = Stopwatch.StartNew();
                array = BubbleSort(array);
                Console.WriteLine($"Відсортований масив");
                foreach (var f in array)
                {
                    Console.WriteLine(f);
                }

                time.Stop();
                b1 = time.Elapsed.TotalSeconds;
                Console.WriteLine($"Час для Сортування бульбашкою = {time.Elapsed.TotalSeconds} s");

                System.Console.WriteLine("Input ok to see graphs from first\n");
                str = System.Console.ReadLine();
                if (str == "ok")
                {
                    break;
                }

            }
            FreeConsole();
        }

        

        


        private void button1_Click(object sender, EventArgs e)
        {
            double y6 = 1;


            for (int i = 0; i < 7; i++)
            {
                this.chart1.Series[i].Points.Clear();
            }

            for (int i = 0; i <= 50; i++)
            {
                

                if (i > 0 && i <= 5)
                {
                    y6 = y6 * i;
                    this.chart1.Series[5].Points.AddXY(i, y6);
                }

                this.chart1.Series[0].Points.AddXY(i, i);

                if (i > 0)
                {
                    this.chart1.Series[2].Points.AddXY(i, i * Math.Log(i));
                    this.chart1.Series[1].Points.AddXY(i, Math.Log(i));
                }
                if (i == 0)
                {
                    this.chart1.Series[2].Points.AddXY(i, i);
                    this.chart1.Series[1].Points.AddXY(i, i);
                    this.chart1.Series[5].Points.AddXY(i, i);
                }

                this.chart1.Series[3].Points.AddXY(i, i ^ 2);
                this.chart1.Series[4].Points.AddXY(i, i * 2);



            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 7; i++)
            {
                this.chart1.Series[i].Points.Clear();
            }
            this.chart1.Series[6].Points.AddXY(f1,f2);
            this.chart1.Series[7].Points.AddXY(12, b1);

        }
    }
}
