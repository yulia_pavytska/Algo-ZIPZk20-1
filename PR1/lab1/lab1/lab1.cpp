﻿#include <iostream>
#include <cstdlib>

using namespace std;

struct MyStruct
{
    unsigned short year : 12;
    unsigned short month : 4;
    unsigned char day : 5;
    unsigned char hour : 5;
    unsigned char min : 6;
    unsigned char sec : 6;
};



struct st2
{
    unsigned short low : 8;
    unsigned short hight : 8;
};

struct nambit
{
    unsigned short u_s1 : 1;
    unsigned short u_s2 : 1;
    unsigned short u_s3 : 1;
    unsigned short u_s4 : 1;
    unsigned short u_s5 : 1;
    unsigned short u_s6 : 1;
    unsigned short u_s7 : 1;
    unsigned short u_s8 : 1;
    unsigned short u_s9 : 1;
    unsigned short u_s10 : 1;
    unsigned short u_s11 : 1;
    unsigned short u_s12 : 1;
    unsigned short u_s13 : 1;
    unsigned short u_s14 : 1;
    unsigned short u_s15 : 1;
    unsigned short u_s16 : 1;
};

union un_2
{
    signed short a;
    st2 namb;
    nambit nambbyt;
};

union task4 {
    union num {
        struct n {
            unsigned short a0 : 1;
            unsigned short a1 : 1;
            unsigned short a2 : 1;
            unsigned short a3 : 1;
            unsigned short a4 : 1;
            unsigned short a5 : 1;
            unsigned short a6 : 1;
            unsigned short a7 : 1;
            unsigned short a8 : 1;
            unsigned short a9 : 1;
            unsigned short a10 : 1;
            unsigned short a11 : 1;
            unsigned short a12 : 1;
            unsigned short a13 : 1;
            unsigned short a14 : 1;
            unsigned short a15 : 1;
        }numer;
        signed short count;
    }num1;

    union numbers {
        struct number {
            unsigned short b0 : 1;
            unsigned short b1 : 1;
            unsigned short b2 : 1;
            unsigned short b3 : 1;
            unsigned short b4 : 1;
            unsigned short b5 : 1;
            unsigned short b6 : 1;
            unsigned short b7 : 1;
            unsigned short b8 : 1;
            unsigned short b9 : 1;
            unsigned short b10 : 1;
            unsigned short b11 : 1;
            unsigned short b12 : 1;
            unsigned short b13 : 1;
            unsigned short b14 : 1;
            unsigned short b15 : 1;
            unsigned short b16 : 1;
            unsigned short b17 : 1;
            unsigned short b18 : 1;
            unsigned short b19 : 1;
            unsigned short b20 : 1;
            unsigned short b21 : 1;
            unsigned short b22 : 1;
            unsigned short b23 : 1;
            unsigned short b24 : 1;
            unsigned short b25 : 1;
            unsigned short b26 : 1;
            unsigned short b27 : 1;
            unsigned short b28 : 1;
            unsigned short b29 : 1;
            unsigned short b30 : 1;
            unsigned short b31 : 1;
        }number;

        struct num3 {
            unsigned short val1 : 8;
            unsigned short val2 : 8;
            unsigned short val3 : 8;
            unsigned short val4 : 8;
        }num4;
        float val;
    }num2;
}task4;

















int main()
{

    MyStruct structure_time;
    tm t;
    cout << "Size of my struct: " << sizeof(structure_time) << endl;
    cout << "Size of tm struct: " << sizeof(t) << endl;
    structure_time.year = 2021;
    structure_time.month = 05;
    structure_time.day = 25;
    structure_time.hour = 13;
    structure_time.min = 57;
    structure_time.sec = 13;
    cout << "Year =  " << structure_time.year << endl;
    cout << "Month = " << structure_time.month << endl;
    cout << "Day = " << (int)structure_time.day << endl;
    cout << "Hour = " << (int)structure_time.hour << endl;
    cout << "Min = " << (int)structure_time.min << endl;
    cout << "Sec = " << (int)structure_time.sec << endl << endl;



    short a = 10;
    un_2 un22;
    un22.a = a;
    cout << "struct a - " << un22.a << endl;
    cout << hex << un22.a << " union low,hight - " << un22.namb.hight << "," << un22.namb.low << endl;
    cout << "union bit " << un22.nambbyt.u_s1 << " " << un22.nambbyt.u_s2 << " " << un22.nambbyt.u_s3 << " " << un22.nambbyt.u_s4 << " " << un22.nambbyt.u_s5 << " " << un22.nambbyt.u_s6 << " " << un22.nambbyt.u_s7 << " " << un22.nambbyt.u_s8 << " " << endl;
    cout << "union bit " << un22.nambbyt.u_s9 << " " << un22.nambbyt.u_s10 << " " << un22.nambbyt.u_s11 << " " << un22.nambbyt.u_s12 << " " << un22.nambbyt.u_s13 << " " << un22.nambbyt.u_s14 << " " << un22.nambbyt.u_s15 << " " << un22.nambbyt.u_s16 << " " << endl;
    signed char op_a = 5 + 127;
    signed char op_b = 2 - 3;
    signed char op_c = -120 - 34;
    unsigned char op_e = (unsigned char)(-5);
    signed char op_d = 56 & 38;
    signed char op_f = 56 | 38;

    cout << endl << "5 + 127 = " << (int)op_a << endl;
    cout << "2 - 3 = " << (int)op_b << endl;
    cout << "-120 - 34 = " << (int)op_c << endl;
    cout << "(unsigned char)(-5) = " << (int)op_e << endl;
    cout << "56 & 38 = " << (int)op_d << endl;
    cout << "56 | 38 = " << (int)op_f << endl;

    system("chcp 1251");
    system("cls");
    float a;
    printf("Enter element = ");
    scanf_s("%f", &task4.num2.val);
    printf("%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
        task4.num2.number.b31, task4.num2.number.b30, task4.num2.number.b29, task4.num2.number.b28, task4.num2.number.b27, task4.num2.number.b26,
        task4.num2.number.b25, task4.num2.number.b24, task4.num2.number.b23, task4.num2.number.b22, task4.num2.number.b21, task4.num2.number.b20,
        task4.num2.number.b19, task4.num2.number.b18, task4.num2.number.b17, task4.num2.number.b16, task4.num2.number.b15, task4.num2.number.b14,
        task4.num2.number.b13, task4.num2.number.b12, task4.num2.number.b11, task4.num2.number.b10, task4.num2.number.b9, task4.num2.number.b8,
        task4.num2.number.b7, task4.num2.number.b6, task4.num2.number.b5, task4.num2.number.b4, task4.num2.number.b3, task4.num2.number.b2,
        task4.num2.number.b1, task4.num2.number.b0);
    printf("\n");
    printf("%d %d %d %d",


        task4.num2.num4.val1, task4.num2.num4.val2, task4.num2.num4.val3,
        task4.num2.num4.val4);
    printf("\n");
    printf("Mantisa: %d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n", task4.num2.number.b22, task4.num2.number.b21, task4.num2.number.b20,
        task4.num2.number.b19, task4.num2.number.b18, task4.num2.number.b17, task4.num2.number.b16, task4.num2.number.b15, task4.num2.number.b14,
        task4.num2.number.b13, task4.num2.number.b12, task4.num2.number.b11, task4.num2.number.b10, task4.num2.number.b9, task4.num2.number.b8,
        task4.num2.number.b7, task4.num2.number.b6, task4.num2.number.b5, task4.num2.number.b4, task4.num2.number.b3, task4.num2.number.b2,
        task4.num2.number.b1, task4.num2.number.b0);
    printf("Znak: %d\n", task4.num2.number.b31);
    printf("Stepen: %d%d%d%d%d%d%d%d\n", task4.num2.number.b30, task4.num2.number.b29,
        task4.num2.number.b28, task4.num2.number.b27, task4.num2.number.b26, task4.num2.number.b25, task4.num2.number.b24,
        task4.num2.number.b23);
    printf("Structure takes up space of  %d byte", sizeof(task4));
    return 0;
}











