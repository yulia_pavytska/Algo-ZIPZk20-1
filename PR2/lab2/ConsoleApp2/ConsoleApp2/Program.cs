﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomGenerator
{
    class Program
    {
        public static void LineRand(ulong x0, int[] arrRand, int a = 22695477, int b = 1, int k = 0)
        {
            ulong m = (ulong)Math.Pow(2, 32);

            if (k < 4000)
            {
                x0 = ((ulong)a * x0 + (ulong)b) % m;
                k++;
                LineRand(x0, arrRand, a, b, k);
                arrRand[k - 1] = (int)(x0 % 200);
            }
            else
            {
                return;
            }
        }

        public static int Checked(int[] array, int num)
        {
            int freq = 0;
            for (int i = 0; i < 4000; i++)
            {
                if (array[i] == num)
                {
                    freq++;
                }
            }
            return freq;
        }

        public static double MathExp(int[] arr, int[] frequency)
        {
            double mathExp = 0;
            for (int i = 0; i < 4000; i++)
            {
                mathExp += arr[i] * frequency[i] / 4000;
            }
            return mathExp;
        }

        public static double Sigma(int[] arr, int[] frequency)
        {
            double Sigma = 0;
            float[] floatArr = new float[4000];
            for (int i = 0; i < 4000; i++)
            {
                floatArr[i] = (float)arr[i];
            }
            float avg = floatArr.Average();
            for (int i = 0; i < 4000; i++)
            {
                Sigma = Math.Pow((avg - arr[i]), 2) / 4000;
            }
            Sigma = Math.Sqrt(Sigma);
            return Sigma;
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            ulong x0 = 0;
            int[] arrRand = new int[4000];
            LineRand(x0, arrRand);
            Array.Reverse(arrRand);
            int[] frequency = new int[4000];
            for (int i = 0; i < arrRand.Length; i++)
            {
                frequency[i] = Checked(arrRand, arrRand[i]);
                Console.WriteLine($"{i + 1} - {arrRand[i]} - {frequency[i]} - {(double)frequency[i] / 4000}");
            }

            Console.WriteLine($"Математичне очікування:{MathExp(arrRand, frequency)}");
            double dyspersion = 0;
            for (int i = 0; i <= 250; i++)
            {
                dyspersion += Math.Pow((i - MathExp(arrRand, frequency)), 2) * frequency[i] / 4000;
            }
            Console.WriteLine($"Дисперсія псевдовипадкових величин: {dyspersion}");
            Console.WriteLine($"Середньоквадратичне відхилення: {Sigma(arrRand, frequency)}");
            Console.ReadKey();
        }
    }
}
