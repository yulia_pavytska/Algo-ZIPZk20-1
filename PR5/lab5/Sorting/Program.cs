﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections;

namespace Sorting
{
    class Program
    {
        public class Node 
        {
            public int data;
            public Node next, prev;
        }

        static Node getNode(int data)
        {
            Node newNode = new Node();

            newNode.data = data;
            newNode.prev = newNode.next = null;
            return newNode;
        }

        static Node SortedInsert(Node list, Node newNode)
        {
            Node current;

            if (list == null)
                list = newNode;

            else if (list.data >= newNode.data)
            {
                newNode.next = list;
                newNode.next.prev = newNode;
                list = newNode;
            }

            else
            {
                current = list;

                while (current.next != null && current.next.data < newNode.data)
                    current = current.next;

                newNode.next = current.next;

                if (current.next != null)
                    newNode.next.prev = newNode;

                current.next = newNode;
                newNode.prev = current;

            }
            return list;
        }
        
        static void FillArray(ref int[] arr)
        {
            Random rand = new Random();

            for (int i = 0; i < arr.Length; i++)
                arr[i] = rand.Next(-150, 150);
        }

        static void SelectionMethod(ref int[] arr)
        {
            for(int i=0; i<arr.Length-1; i++)
            {
                int min = i;

                for(int j=i+1; j<arr.Length; j++)
                {
                    if (arr[j] < arr[min])
                        min = j;
                }
                int tmp = arr[i];
                arr[i] = arr[min];
                arr[min] = tmp;
            }
        }

        static void ArrInsertion(ref int[] arr)
        {
            for(int i=1; i<arr.Length; i++)
            {
                int j;
                int tmp = arr[i];

                for(j=i-1; j>=0; j--)
                {
                    if (arr[j] < tmp)
                        break;
                    arr[j + 1] = arr[j];
                }

                arr[j + 1] = tmp;
            }
        }

        static void ShellsMethod(ref int[] arr)
        {
            int step, i, j;
            int[] inc = new int[40];

            int p1 = 1, p2 = 1, p3 = 1, st = -1;
            do
            {
                if (++st % 2 == 0)
                    inc[st] = 8 * p1 - 6 * p2 + 1;
                else
                {
                    inc[st] = 9 * p1 - 9 * p3 + 1;
                    p2 *= 2;
                    p3 *= 2;
                }
                p1 *= 2;
            } while (3 * inc[st] < inc.Length);

            st = st > 0 ? --st : 0;

            while (st >= 0)
            {
                step = inc[st--];

                for (i = step; i < arr.Length; i++)
                {
                    int temp = arr[i];

                    for (j = i - step; (j >= 0) && (arr[j] > temp); j -= step)
                        arr[j + step] = arr[j];
                    arr[j + step] = temp;
                }
            }
        }

        static void Main(string[] args)
        {
            Stopwatch sw;
            int[] arr500 = new int[500];
            int[] arr1000 = new int[1000];
            int[] arr2000 = new int[2000];
            LinkedList<int> lst = new LinkedList<int>();

            Console.WriteLine("Сортування вибором(масив):");

            for(int i=0; i<arr500.Length; i++)
            {
                FillArray(ref arr500);
            }

            sw = Stopwatch.StartNew();
            for(int i=0; i<arr500.Length; i++)
            {
                SelectionMethod(ref arr500);
            }
            sw.Stop();

            Console.WriteLine($"Час для масиву, 500 елементів -> {sw.Elapsed.TotalSeconds} секунд");

            for (int i = 0; i < arr1000.Length; i++)
            {
                FillArray(ref arr1000);
            }

            sw = Stopwatch.StartNew();
            for (int i = 0; i < arr1000.Length; i++)
            {
                SelectionMethod(ref arr1000);
            }
            sw.Stop();

            Console.WriteLine($"Час для масиву, 1000 елементів -> {sw.Elapsed.TotalSeconds} секунд");

            for (int i = 0; i < arr2000.Length; i++)
            {
                FillArray(ref arr2000);
            }
            sw = Stopwatch.StartNew();
            for (int i = 0; i < arr2000.Length; i++)
            {
                SelectionMethod(ref arr2000);
            }
            sw.Stop();

            Console.WriteLine($"Час для масиву, 2000 елементів -> {sw.Elapsed.TotalSeconds} секунд");
            Console.WriteLine();
            Console.WriteLine("Сортування вставками(масив):");

            for (int i = 0; i < arr500.Length; i++)
            {
                FillArray(ref arr500);
            }

            sw = Stopwatch.StartNew();
            for (int i = 0; i < arr500.Length; i++)
            {
                SelectionMethod(ref arr500);
            }
            sw.Stop();

            Console.WriteLine($"Час для масиву, 500 елементів -> {sw.Elapsed.TotalSeconds} секунд");

            for (int i = 0; i < arr1000.Length; i++)
            {
                FillArray(ref arr1000);
            }

            sw = Stopwatch.StartNew();
            for (int i = 0; i < arr1000.Length; i++)
            {
                ArrInsertion(ref arr1000);
            }
            sw.Stop();

            Console.WriteLine($"Час для масиву, 1000 елементів -> {sw.Elapsed.TotalSeconds} секунд");

            for (int i = 0; i < arr2000.Length; i++)
            {
                FillArray(ref arr2000);
            }
            sw = Stopwatch.StartNew();
            for (int i = 0; i < arr2000.Length; i++)
            {
                ArrInsertion(ref arr2000);
            }
            sw.Stop();

            Console.WriteLine($"Час для масиву, 2000 елементів -> {sw.Elapsed.TotalSeconds} секунд");
            Console.WriteLine();
            Console.WriteLine("Сортування методом Шелла(масив):");

            for (int i = 0; i < arr500.Length; i++)
            {
                FillArray(ref arr500);
            }

            sw = Stopwatch.StartNew();
            for (int i = 0; i < arr500.Length; i++)
            {
                ShellsMethod(ref arr500);
            }
            sw.Stop();

            Console.WriteLine($"Час для масиву, 500 елементів -> {sw.Elapsed.TotalSeconds} секунд");

            for (int i = 0; i < arr1000.Length; i++)
            {
                FillArray(ref arr1000);
            }

            sw = Stopwatch.StartNew();
            for (int i = 0; i < arr1000.Length; i++)
            {
                ShellsMethod(ref arr1000);
            }
            sw.Stop();

            Console.WriteLine($"Час для масиву, 1000 елементів -> {sw.Elapsed.TotalSeconds} секунд");

            for (int i = 0; i < arr2000.Length; i++)
            {
                FillArray(ref arr2000);
            }
            sw = Stopwatch.StartNew();
            for (int i = 0; i < arr2000.Length; i++)
            {
                ShellsMethod(ref arr2000);
            }
            sw.Stop();

            Console.WriteLine($"Час для масиву, 2000 елементів -> {sw.Elapsed.TotalSeconds} секунд");
            Console.WriteLine();
            Console.WriteLine("Сортування вставками(список):");

            Random rand = new Random();
            Node list = null;

            sw = Stopwatch.StartNew();
            for (int i = 0; i < 500; i++)
            {
                list = SortedInsert(list, getNode(rand.Next(-150, 150)));
            }
            sw.Stop();

            Console.WriteLine($"Час для списку, 500 елементів -> {sw.Elapsed.TotalSeconds} секунд");

            sw = Stopwatch.StartNew();
            for (int i = 0; i < 1000; i++)
            {
                list = SortedInsert(list, getNode(rand.Next(-150, 150)));
            }
            sw.Stop();

            Console.WriteLine($"Час для списку, 1000 елементів -> {sw.Elapsed.TotalSeconds} секунд");

            sw = Stopwatch.StartNew();
            for (int i = 0; i < 2000; i++)
            {
                list = SortedInsert(list, getNode(rand.Next(-150, 150)));
            }
            sw.Stop();

            Console.WriteLine($"Час для списку, 2000 елементів -> {sw.Elapsed.TotalSeconds} секунд");
            Console.ReadKey();
        }
    }
}
