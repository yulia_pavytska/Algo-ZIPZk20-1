﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace algoslab6
{
    public partial class Form1 : Form
    {


        [DllImport("kernel32.dll", SetLastError = true)]
        //[return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool AllocConsole();

        [DllImport("kernel32.dll", SetLastError = true)]
        //[return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool FreeConsole();

        string str;

        static void Fill(ref int[] arr)
        {
            Random rnd = new Random();

            for (int i = 0; i < arr.Length; i++)
                arr[i] = rnd.Next(0, 400);
        }
        static void FillF(ref float[] arr)
        {
            Random rnd = new Random();

            for (int i = 0; i < arr.Length; i++)
                arr[i] = rnd.Next(-100, 10);
        }

        static void FillShort(ref short[] arr)
        {
            Random rnd = new Random();

            for (int i = 0; i < arr.Length; i++)
                arr[i] = Convert.ToInt16(rnd.Next(-10, 100));
        }




        static void Shells(ref int[] arr)
        {
            {
                int j;
                int step = arr.Length / 2;
                while (step > 0)
                {
                    for (int i = 0; i < (arr.Length - step); i++)
                    {
                        j = i;
                        while ((j >= 0) && (arr[j] > arr[j + step]))
                        {
                            var temp = arr[j];
                            arr[j] = arr[j + step];
                            arr[j + step] = temp;
                            j = j - step;
                        }
                    }
                    step = step / 2;
                }
            }
        }


        static Int32 add2pyramid(float[] arr, Int32 i, Int32 N)
        {
            Int32 imax;
            float buf;
            if ((2 * i + 2) < N)
            {
                if (arr[2 * i + 1] < arr[2 * i + 2]) imax = 2 * i + 2;
                else imax = 2 * i + 1;
            }
            else imax = 2 * i + 1;
            if (imax >= N) return i;
            if (arr[i] < arr[imax])
            {
                buf = arr[i];
                arr[i] = arr[imax];
                arr[imax] = buf;
                if (imax < N / 2) i = imax;
            }
            return i;
        }

        static void Pyramid(float[] arr, Int32 len)
        {
            for (Int32 i = len / 2 - 1; i >= 0; --i)
            {
                long prev_i = i;
                i = add2pyramid(arr, i, len);
                if (prev_i != i) ++i;
            }

            float buf;
            for (Int32 k = len - 1; k > 0; --k)
            {
                buf = arr[0];
                arr[0] = arr[k];
                arr[k] = buf;
                Int32 i = 0, prev_i = -1;
                while (i != prev_i)
                {
                    prev_i = i;
                    i = add2pyramid(arr, i, k);
                }
            }
        }

        static short[] Counting(short[] array)
        {
            var min = array[0];
            var max = array[0];
            foreach (short element in array)
            {
                if (element > max)
                {
                    max = element;
                }
                else if (element < min)
                {
                    min = element;
                }
            }

            var correctionFactor = min != 0 ? -min : 0;
            max += (short)correctionFactor;

            var count = new int[max + 1];
            for (var i = 0; i < array.Length; i++)
            {
                count[array[i] + correctionFactor]++;
            }

            var index = 0;
            for (var i = 0; i < count.Length; i++)
            {
                for (var j = 0; j < count[i]; j++)
                {
                    array[index] = Convert.ToInt16(i - correctionFactor);
                    index++;
                }
            }

            return array;
        }




        double[] shell = new double[4];
        double[] pyramid = new double[4];
        double[] counts = new double[4];
        double[] nums = new double[4] { 10, 100, 500, 1000 };

        public Form1()
        {


            InitializeComponent();
            while (true)
            {

                AllocConsole();
                int[] arr10 = new int[10];
                int[] arr100 = new int[100];
                int[] arr500 = new int[500];
                int[] arr1000 = new int[1000];
                float[] frr10 = new float[10];
                float[] frr100 = new float[100];
                float[] frr500 = new float[500];
                float[] frr1000 = new float[1000];
                short[] srr10 = new short[10];
                short[] srr100 = new short[100];
                short[] srr500 = new short[500];
                short[] srr1000 = new short[1000];



                Stopwatch time;




                Console.WriteLine();
                Console.WriteLine("Шелл");

                Fill(ref arr10);

                time = Stopwatch.StartNew();
                for (int i = 0; i < frr10.Length; i++)
                {
                    Shells(ref arr10);
                }
                time.Stop();
                shell[0] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Время для масива  10   = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr100);

                time = Stopwatch.StartNew();
                for (int i = 0; i < frr100.Length; i++)
                {
                    Shells(ref arr100);
                }
                time.Stop();
                shell[1] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Время для масива  100   = {time.Elapsed.TotalSeconds} s");
                Fill(ref arr500);

                time = Stopwatch.StartNew();
                for (int i = 0; i < frr500.Length; i++)
                {
                    Shells(ref arr500);
                }
                time.Stop();
                shell[2] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Время для масива  500   = {time.Elapsed.TotalSeconds} s");
                Fill(ref arr1000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < frr1000.Length; i++)
                {
                    Shells(ref arr1000);
                }
                time.Stop();
                shell[3] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Время для масива  1000   = {time.Elapsed.TotalSeconds} s");


                Console.WriteLine();

                Console.WriteLine("Пирамиды");

                FillF(ref frr10);

                time = Stopwatch.StartNew();
                for (int i = 0; i < frr10.Length; i++)
                {
                    Pyramid(frr10, frr10.Length);
                }
                time.Stop();
                pyramid[0] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Время для масива  10   = {time.Elapsed.TotalSeconds} s");

                FillF(ref frr100);

                time = Stopwatch.StartNew();
                for (int i = 0; i < frr100.Length; i++)
                {
                    Pyramid(frr100, frr100.Length);
                }
                time.Stop();
                pyramid[1] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Время для масива  100   = {time.Elapsed.TotalSeconds} s");

                FillF(ref frr500);

                time = Stopwatch.StartNew();
                for (int i = 0; i < frr500.Length; i++)
                {
                    Pyramid(frr500, frr500.Length);
                }
                time.Stop();
                pyramid[2] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Время для масива  500   = {time.Elapsed.TotalSeconds} s");
                FillF(ref frr1000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < frr1000.Length; i++)
                {
                    Pyramid(frr1000, frr1000.Length);
                }
                time.Stop();
                pyramid[3] = time.Elapsed.TotalSeconds;
                Console.WriteLine($"Время для масива  1000   = {time.Elapsed.TotalSeconds} s");


                Console.WriteLine();

                Console.WriteLine("Подсчётом");

                FillShort(ref srr10);

                time = Stopwatch.StartNew();
                for (int i = 0; i < srr10.Length; i++)
                {
                    Counting(srr10);
                }
                time.Stop();
                counts[0] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Время для масива  10   = {time.Elapsed.TotalSeconds} s");
                FillShort(ref srr100);

                time = Stopwatch.StartNew();
                for (int i = 0; i < srr100.Length; i++)
                {
                    Counting(srr100);
                }
                time.Stop();
                counts[1] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Время для масива  100   = {time.Elapsed.TotalSeconds} s");
                FillShort(ref srr500);

                time = Stopwatch.StartNew();
                for (int i = 0; i < srr500.Length; i++)
                {
                    Counting(srr500);
                }
                time.Stop();
                counts[2] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Время для масива  500   = {time.Elapsed.TotalSeconds} s");
                FillShort(ref srr1000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < srr1000.Length; i++)
                {
                    Counting(srr1000);
                }
                time.Stop();
                counts[3] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Время для масива  1000   = {time.Elapsed.TotalSeconds} s");

                System.Console.WriteLine("Напишите ok что бы посмотреть график\n");
                str = System.Console.ReadLine();
                if (str == "ok")
                {
                    break;
                }

            }
            FreeConsole();
        }


        private void button1_Click(object sender, EventArgs e)
        {


            for (int i = 0; i < 3; i++)
            {
                this.chart1.Series[i].Points.Clear();
            }


            for (int i = 0; i < 4; i++)
            {
                this.chart1.Series[0].Points.AddXY(shell[i], nums[i]);
                this.chart1.Series[1].Points.AddXY(pyramid[i], nums[i]);
                this.chart1.Series[2].Points.AddXY(counts[i], nums[i]);
            }
        }


    }
}
